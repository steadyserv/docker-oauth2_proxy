# oauth2_proxy Docker Image

Docker image providing [oauth2_proxy][1].

## Usage Examples

Running with command-line flags:
```
docker run -d \
  --name oauth2_proxy \
  --network host \
  steadyserv/oauth2_proxy:latest \
    -http-address string: [http://] \
    -email-domain="yourcompany.com"  \
    -upstream=http://127.0.0.1:8080/ \
    -cookie-secret=... \
    -cookie-secure=true \
    -provider=... \
    -client-id=... \
    -client-secret=...
```

Running with a bind-mounted config file:
```
docker run -d \
  --name oauth2_proxy \
  --network host \
  --volume:/path/on/host/oauth2_proxy.cfg:/etc/oauth2_proxy.cfg:ro \
  steadyserv/oauth2_proxy:latest \
    -config=/etc/oauth2_proxy.cfg
```

[1]: https://github.com/bitly/oauth2_proxy
